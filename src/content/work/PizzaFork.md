---
title: Pizza Fork 
publishDate: 2023-04-09 00:00:00
img: /assets/Laravel.jpg
img_alt: C#
description: |
  Ce projet était mon second support d'évaluation, je pouvais tomber sur ce projet ou bien la colocation crous et le but était d'ajouter des fonctionnalité précise dans un temps imparti.
tags:
  - FrameWork
  - Dev
  - Evaluation
---


# PIZZAFORK

Suivre :  

```
git clone https://gitlab.com/portfolio-2023-tazimohamed/activite-2-e5-pizzafork.git

Faire un composer update

Copier le fichier .env.example

Le coller à la racine du projet puis renommer ce fichier en .env

Configurer le fichier .env (nom base de donnée : pizzafork,accès à la bdd...)

Créer la base de donnée pizzafork (drop si la bdd existe déjà et la recréer), lui donner le même nom que celui qui a été indiqué dans le fichier .env

php artisan migrate

php artisan db:seed

php artisan key:generate

php artisan storage:link

Et enfin php artisan serve

```

-----

<br><br><br>

----

<br>

Voici l'accueil :  
![](/assets/img/imgpizzaFork/Accueil.png)

voici le cas d'utilisation du site :

![](/assets/img/imgpizzaFork/CasUtilisation.PNG)

<br>

Nous pouvons ajouter/modifier/supprimer des pizzas grâce a différent bouton présent :  
![](/assets/img/imgpizzaFork/Accueil.png)

<br> 

Nous pouvons faire la même chose pour les ingrédients :  
![](/assets/img/imgpizzaFork/Ingredients.png)

<br>

Enfin, nous pouvons ajouter, modifier et supprimer les garnitures d'une pizza en appuyant sur ce bouton dans la liste des pizzas :  
![](/assets/img/imgpizzaFork/Garnitures.png)
