---
title: Projet Colocation Crous
publishDate: 2023-04-09 00:00:00
img: /assets/csharp.jpg
img_alt: C#
description: |
  J'ai eu à rendre ce projet comme support d'évaluation pour mon BTS SIO, le but était d'ajouter des fonctionnalités demandés dans un temps imparti.
tags:
  - WindowsForm
  - Dev
  - User 
  - Base de donnée
---


# Application Colocation CROUS
> Fonctionnement : 
L'Application du CROUS pour une colocatation permet de :
* Gerer des Colocataires
* Gerer des Dépenses
* Répartir Des Dépenses

<br></br>
> Voici les tables de la base de donnée qu'utilisera l'application :
```sql
drop database if exists dbcoloc;
create database dbcoloc;
use dbcoloc;
```
```sql
CREATE TABLE Colocataire (
	id int(11) unsigned not null auto_increment,
	nom VARCHAR(50),
	prenom VARCHAR(50),
	mail varchar(50),
	telephone varchar(20),
	primary key (id)
	);
```
```sql
CREATE TABLE Depense(
	id int(11) unsigned not null auto_increment,
	ladate DateTime,
	texte Varchar(50),
    justificatif varchar (256),
    montant Decimal(10,2),
    reparti boolean,
    id_Coloc int(11) unsigned not null,
	Primary key (id),
    Foreign key (idColoc) REFERENCES Colocataire(id)
	);
```

Voici l'interface permettant d'accéder aux différentes fonctionnalités :

![](/assets/img/imgCrous/FMenu.png)

![](/assets/img/imgCrous/CasUtilisation.PNG)
<br></br>

> **Gerer les colocataires :**

![](/assets/img/imgCrous/CasUtilisationColoc.PNG)

<br></br>

Voici l'interface permettant de consulter la liste des colocataires et permettant d'ajouter, modifier ou encore de supprimer un colocataire :
![](/assets/img/imgCrous/FGererColoc.png)

<br></br>

### AJOUT COLOCATAIRE :

On peut donc Ajouter des colocataires en appuyant sur le bouton Ajouter nous redirigeant vers une nouvelle fenêtre : 

![](/assets/img/imgCrous/FAjouterColoc.png)

Si il y a une information manquante, une MessageBox est affichée pour demander de remplir les conditions manquantes. De plus, grâce à des restrictions à la saisie, des chiffres ne peuvent entrés pour le nom par exemple.

<br></br>

### MODIFICATION COLOCATAIRE :
On peut également modifier les informations d'un colocataire en appuyant sur le bouton Modifier. Attention il faut sélectionner un colocataire avant d'appuyer sur Modifier.

![](/assets/img/imgCrous/FGererColoc.png)

nous redirigeant ainsi vers une nouvelle fenêtre :

![](/assets/img/imgCrous/FModifColoc.png)

De même, si une des informations entrées ne sont pas correctes, la page ne se fermera pas attendant ainsi les bonnes informations :


<br></br>

### SUPPRESSION COLOCATAIRE :

Il est aussi possible de supprimer un colocataire, pour cela il faut sélectionner le colocataire puis appuyer sur Supprimer.

![](/assets/img/imgCrous/FGererColoc.png)
<br></br><br></br>

<br></br>

</br>

Enfin, on ne peut pas supprimer des colocataires qui a des dépenses à répartir.

## **Gerer les dépenses :**
---
![](/assets/img/imgCrous/CasUtilisationGererDep.PNG)


Voici la page d'accueil pour gérer les dépenses, on peut voir le total des dépenses affichées dans la ListBox, on peut aussi voir une comboBox permettant de filtrer les dépenses par rapport à leur date.

Voici les dépenses présents :  
![](/assets/img/imgCrous/FGererDepense.png)

<br></br>

### AJOUT DEPENSE :
![](/assets/img/imgCrous/FAjouterDepense.png)

<br></br>

### MODIFICATION DEPENSE :
En cas d'erreur ou autres, les informations comme la date, le texte, le montant ou encore le fichier justificatif peuvent être modifiés.

![](/assets/img/imgCrous/FModifDep.png)

<br></br>

### SUPPRESSION DEPENSE :
Il suffit de sélectionner la dépense et de cliquer sur le bouton Supprimer


<br></br>



## **Répartir les dépenses :**
---
Cette fonctionnalité permet de calculer ce que chaque personne doit aux autres colocataires par rapport aux dépenses faites pour la colocation.

![](/assets/img/imgCrous/FRepartir.png)

On appuie donc sur le bouton Lancer la Répartition pour être rediriger vers une nouvelle fenêtre :

<br>

On peut voir sur le tableau de gauche les dépenses qui n'ont pas encore été réparties. En effet on pouvait voir dans le tableau de la fenêtre Gerer Depense que les dépenses étaint au nombre de trois :  
<br>
Or on a ici dans le tableau des répartition, zéro dépenses. Cela signifie qu'il n'y pas de dépense à répartir.

<br>

En appuyant sur le bouton Répartir, nous obtiendrons sur le tableau en bas à droite : la somme que doit régler chaque colocataire par rapport à ses dépenses faites pour la colocation.  
<br>
