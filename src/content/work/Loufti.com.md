---
title: Loufti.com 
publishDate: 2022-00-00 00:00:00
img: /assets/logo.png
img_alt: C#
description: |
  J'ai réaliser ce projet en groupe le but était de nous montrer comment développer un site web et en binome afin de comprendre comment bien travailler en équipe
tags:
  - Design
  - Dev
  - Travail d'équipe
---

# "Loufti.com"
 "Loufti.com" est un site vitrine en PHP qui permet l'ajout de mangaka et de manga qui seront ensuite affichée sur le site via des requêtes SQL :
> Cas D'utilisation :

![](/assets/img/imgLoufti/CasUtilisation.PNG)

> Diagramme de classe :

![](/assets/img/imgLoufti/DiagrammeClasse.PNG)


En arrivant sur la page d'accueil, l'utilisateur à la possibilité d'ajouter une oeuvre de son choix en remplissant un formulaire. Pour cela il doit au préalable avoir crée ou selectionner un mangaka :
![](/assets/img/imgLoufti/Accueil.PNG)

Création d'un Mangaka :
![](/assets/img/imgLoufti/Mangaka.PNG)
![](/assets/img/imgLoufti/AjoutOeuvre.png/)

Après ajout de l'oeuvre vous pouvez contempler les différents mangas et leurs caractéristiques précises :
![](/assets/img/imgLoufti/Oeuvre.png)



    
    

    
 

    



